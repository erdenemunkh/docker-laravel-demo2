require("./bootstrap");

import { createApp } from "vue";
import { createRouter, createWebHistory } from "vue-router";
import App from "./App.vue";
import Home from "./components/HomeComponent.vue";

const About = { template: "<div>About</div>" };

const routes = [
    { path: "/", component: Home },
    { path: "/about", component: About },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

createApp({
    components: {
        App,
    },
})
    .use(router)
    .mount("#app");
